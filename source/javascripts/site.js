document.addEventListener("DOMContentLoaded",function(){
  let carousels = document.querySelectorAll('.slideshow');

  function Carousel(el){
    this.inner  = el.querySelector('.slideshow_slider')
    this.bubblesContainer = el.querySelector('.slideshow_bubbles')
    this.images = el.querySelectorAll('img') 
    this.prev   = el.querySelector('.prev')
    this.next   = el.querySelector('.next')
    this.image_width = this.images[0].offsetWidth
    this.images_array = Array.from(this.images)
    this.total_width = this.image_width * this.images_array.length
    this.pages = this.images.length - 1
    this.bubbles = []
    this.currentImageIndex = 0
    this.activeImage = 1
    this.animating = false
    this.w = this.image_width
    this.offset = (this.w / 3 )* 2
    this.init = function() {
      this.showImgs(this.inner,this.pages,this.images_array)
      this.updateActiveImage()
      this.setupBubbles(this.images,this.bubblesContainer,el)
      this.next.addEventListener("click", this.nextEv.bind(this))
      this.prev.addEventListener('click',this.prevEv.bind(this))
    }

    this.showImgs = function(container,pages,array) {
      TweenMax.set(this.inner,{
        x: -(this.offset)
      })
      for(let i = 0;i < pages; i++){
        let clone = array[i].cloneNode(true);
        container.appendChild(clone);
        this.images_array.push(clone)
      }
    }

    this.animateSlider = function(direction,el,w,reset){
      this.animating = true;
      let target = '';
      if ( reset < 0 ) {
        this.currentImageIndex = this.pages;
        this.activeBubbles(); 
        this.resetSlider(el,this.offset)
      } 
      if ( reset == 1) {
        this.currentImageIndex = 0;
        this.activeBubbles()   
        this.images_array[this.images_array.length - 3].classList.remove("active")
        this.images_array[this.images_array.length - 2].classList.add("active")
        this.images_array[1].classList.add("active")
      }
      if(direction == 1){
        target = `-=${w}`;
      } else {
        target = `+=${w}`;
      }
      TweenMax.to(el,1,{
        x: target,
        onComplete: () => {
          this.animating = false;
          if ( reset == 1 ) {
            this.resetSlider(el,-(this.offset),0)
          }
        }
      })
    }

    this.nextEv = function(){
      if (this.animating){
        return false
      } 
      this.currentImageIndex++;
      if (this.currentImageIndex == (this.pages + 1)){
        this.animateSlider(1,this.inner,this.w,1)
      } else {
        this.activeBubbles();
        this.animateSlider(1,this.inner,this.w,0);
      }
    }

    this.prevEv = function(){
      if (this.animating){
        return false
      } 
      this.currentImageIndex--;
      if ( this.currentImageIndex < 0 ) {
        this.animateSlider(-1,this.inner,this.w,-1)
      } else {
        this.activeBubbles()
        this.animateSlider(-1,this.inner,this.w,0);
      }
    }

    this.goTo = function(i){
      if (this.animating){
        return false;
      }
      TweenMax.to(this.inner,1,{
        x: -((this.w * i)+this.offset)
      })
      this.currentImageIndex = i;
      this.activeBubbles()
    }

    this.resetSlider = function(el,x,direction){
      if (direction == 0){

          TweenMax.set(el,{
          x : x
        })
      } else{
          TweenMax.set(el,{
          x : -(this.total_width+this.offset)
        })
      }
    }

    this.setupBubbles = function(){
      for(let i = 0;i < this.images.length;i++){
        let b = document.createElement('span');
        b.classList.add('dot');
        if(i == 0){
          b.classList.add('active');
        }
        this.bubblesContainer.appendChild(b);
        this.bubbles.push(b);

      this.images_array[1].classList.add("active")
        b.addEventListener('click',(e) => { 
          this.goTo(i);
        });
      }
    }

    this.updateActiveImage = function(new_position) {
      this.images_array.forEach( (image, index) => {
        if ( index == new_position ) {
          image.classList.add("active")
        } else {
          image.classList.remove("active")
        }
      })
    }

    this.activeBubbles = function(){
      this.updateActiveImage(this.currentImageIndex + 1)
      this.bubbles.forEach( (b, i ) => {
      if(i === this.currentImageIndex){
        b.classList.add('active')
        } else {
        b.classList.remove('active')
        }
      })
    }
  }

    [].forEach.call(carousels,function (el){
      let ccc = new Carousel(el)
      ccc.init()
    })
});